

//if Statements


let numA = -1

if(numA<0){
	console.log("Hello")
}

let city = "New York"

if(city == "New York"){
	console.log("Welcome to New York City")
}

//else-if
let numB = 1

if(numA > 0){
	console.log("Hello")
}
else if (numB > 0){
	console.log("World")
}

city = "Tokyo"

if(city === "New York"){
	console.log("Welcome to New York City")
}
else if (city === "Tokyo"){
	console.log("Welcome to Tokyo")
}

//else

if(numA > 0){
	console.log("Hello")
}
else if (numB === 0){
	console.log("World")
}
else {
	console.log("Again")
}


/*

	Mini-Activity:
		Create a conditional statement that if height is below 150, display "Did not pass mini height req."

		If above or equal 150, display "Passed the minimum height req."
		


*/

function checkHeight(height){
	if(height>=150){
		console.log("Passed the minimum height req.")
	}
	else{
		console.log("Did not pass mini height req.")
	}
}

checkHeight(149)

let message = "No message."
console.log(message)


function determineTyphoonIntensity(windSpeed){
	if(windSpeed<30)
		return 'Not a typhoon yet'
	else if (windSpeed<=61)
		return 'Tropical depression detected'
	else if (windSpeed>=62 && windSpeed <=88)
		return 'Tropical storm detected'
	else if (windSpeed>=89 && windSpeed <=117)
		return 'Severe Tropical Storm detected'
	else
		return 'Typhoon detected'
}


message = determineTyphoonIntensity(70)
console.log(message)

if(message === "Tropical storm detected")
	console.warn(message)



//Truthy and Falsy

/*
	-In JavaScript a "truthy" value isa value that is considered TRUE when encountered in a boolean context
	-Value


*/

//Truthy examples
if(true){
	console.log("Truthy")
}
if(1){
	console.log("Truthy")
}
if([]){
	console.log("Truthy")
}

//Falsy Examples
if(false){
	console.log("Falsy")
}
if(0){
	console.log("Falsy")
}
if(undefined){
	console.log("Falsy")
}



//Conditional (Ternary) Operator

let ternaryResult = (1<18) ? true : false


console.log("Result of Ternary Operator: " + ternaryResult)

//Multiple statement execution

let name


function isOfLegalAge() {
	name = "John"
	return "You are of the legal age"
}
function isUnderAge(){
	name = "Jane"
	return "You are under the age limit"
}


let age1 = parseInt(prompt("What is your age?"))
console.log(age1)

let legalAge = (age1 > 18) ? isOfLegalAge() : isUnderAge()
console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name)



//Switch Statement


let day = prompt("What day of the week is it today?").toLowerCase()

console.log(day)

switch(day){

	case "monday":
		console.log("The color of the day is red")
		break
	case "tuesday":
		console.log("The color of the day is orange")
		break
	case "wednesday":
		console.log("The color of the day is yellow")
		break
	case "thursday":
		console.log("The color of the day is green")
		break
	case "friday":
		console.log("The color of the day is blue")
		break
	case "saturday":
		console.log("The color of the day is indigo")
		break
	case "sunday":
		console.log("The color of the day is violet")
		break
	default:
		console.log("Please input a valid day")
		break




}


//Try Catch Finally Statement


function showIntensityAlert(windSpeed){

	try{
		alerat(determineTyphoonIntensity(windSpeed))
	}
	catch(error)
	{
		console.log(error)
		console.log(error.message)
	}
	finally {
		alert("Intensity updates will show new alert.")
	}


}

showIntensityAlert(56)
